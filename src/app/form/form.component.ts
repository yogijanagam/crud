import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreatformService } from '../creatform.service';
import { Employee } from '../model/form.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  creatForm !: FormGroup;
  // employeeModel: Employee = new Employee();
  employeeModel: Employee = new Employee();
  employee !: any;
  constructor(private serviceref: CreatformService,
    private fb: FormBuilder) {
    this.creatForm = this.fb.group({
      firstname: ['',Validators.required],
      lastname: [''],
      email: [''],
      phone: ['']
    })
  }
  ngOnInit(): void {
    this.getAllEmployees();
  }




  postEmployeeData() {
    this.employeeModel.firstname = this.creatForm.value.firstname;
    this.employeeModel.lastname = this.creatForm.value.lastname;
    this.employeeModel.email = this.creatForm.value.email;
    this.employeeModel.phone = this.creatForm.value.phone;

    this.serviceref.postEmployee(this.employeeModel)
      .subscribe(res => {
        alert('Employee Added Successfully')
        this.getAllEmployees();
      },
        err => {
          alert('Somethig Went Wrong')
        })
  }

  getAllEmployees() {
    this.serviceref.getEmployees().subscribe(res => {
      this.employee = res;
    })
  }

  updateEmplioyee(e: any) {
    this.serviceref.updateEmployee(e.id)
      .subscribe(res => {
        alert("Updated Successfully")
        this.getAllEmployees();
      })
  }

}