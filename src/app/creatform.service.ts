import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Employee } from './model/form.model';
@Injectable({
  providedIn: 'root'
})
export class CreatformService {

  constructor(private httpClient: HttpClient) { }

  baseUrl = 'http://localhost:3000/Employees'

  getEmployees(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.baseUrl);
  }
  // getEmployee(id: number): Observable<Employee> {
  //   return this.httpClient.get<Employee>(`${this.baseUrl}/${id}`);
  // }
  postEmployee(employee: Employee): Observable<Employee> {
    return this.httpClient.post<Employee>(this.baseUrl, employee)
  }
  updateEmployee(employee: Employee): Observable<any> {
    return this.httpClient.put<any>(`${this.baseUrl}/${employee}`, employee);
  }
  deleteEmployee(id: number): Observable<any> {
    return this.httpClient.delete<any>(`${this.baseUrl}/${id}`);
  }

}
